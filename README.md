# Prueba DevOps Tottus

Prueba diseñada para ver tus habilidades en el mundo DevOps. La prueba consiste en 2 etapas donde se evaluará las herramientas fundamentales que utilizamos tales como docker, kubernetes, CI/CD. 

## Fase del proceso de selección:
  1. En caso de que cumplas con el perfil técnico del cargo, la primera fase del proceso de selección es uan entrevista tecnica, con el Lider del area Devops.
  2. Como segunda fase comtemplamos una pequeña prueba tecnica, es importante que realices dicha prueba en forma tranquila (tendrás 1 día máximo para poder enviarlo). No te preocupes sino puedes completar todas las fases, para nosotros es importante que realices lo que consideras que tienes experiencia.
  3. Si continúas avanzando con nosotros, el próximo paso es un acercamiento final al equipo con el que estara trabajando, y bienvenida a la familia devops tottus

Una vez completado, no olvide notificar la solución mnlara@tottus.cl

Si tienes alguna duda, puedes escribir a Marvis Lara o enviar un correo electronico a la persona de contacto de tu proveedor.

¡Te deseamos mucho éxito!

## La aplicación
![NodeJs](./img/nodejs.png)

### Instalar Dependencias
```bash
$ npm install
npm WARN deprecated request-promise-native@1.0.9: request-promise-native has been deprecated because it extends the now deprecated request package, see https://github.com/request/request/issues/3142
npm WARN deprecated request@2.88.2: request has been deprecated, see https://github.com/request/request/issues/3142
npm WARN deprecated har-validator@5.1.5: this library is no longer supported
npm WARN deprecated resolve-url@0.2.1: https://github.com/lydell/resolve-url#deprecated
npm WARN deprecated urix@0.1.0: Please see https://github.com/lydell/urix#deprecated
npm notice created a lockfile as package-lock.json. You should commit this file.
npm WARN optional SKIPPING OPTIONAL DEPENDENCY: fsevents@^2.1.2 (node_modules/jest-haste-map/node_modules/fsevents):
npm WARN notsup SKIPPING OPTIONAL DEPENDENCY: Unsupported platform for fsevents@2.3.1: wanted {"os":"darwin","arch":"any"} (current: {"os":"linux","arch":"x64"})
npm WARN basicservice@1.0.0 No repository field.

added 557 packages from 359 contributors and audited 558 packages in 29.142s

20 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
```
### Ejecutar Test

```bash
$ npm run test

> basicservice@1.0.0 test /basic-unit-test
> jest

 PASS  tests/string.test.js
 PASS  reto/tests/sum.test.js
 PASS  tests/sum.test.js
 PASS  reto/tests/string.test.js

Test Suites: 4 passed, 4 total
Tests:       6 passed, 6 total
Snapshots:   0 total
Time:        3.052s
Ran all test suites.
```

### Ejecutar la aplicación

```bash
$ node index.js
Example app listening on port 3000!
```
Podrá acceder a la API localmente en el puerto `3000`.

```bash
$ curl http://localhost:3000/ -s | jq
{
  "msg": "Easy peasy lemon squeezy ;)"
}
$ curl http://localhost:3000/cheers -s | jq
{
  "msg": "You can do it :)"
}
$ curl http://localhost:3000/private -s | jq
{
  "private_token": "VmFzIG11eSBiaWVuIQo="
}
```

### Importante
Las aplicaciones entregadas por el equipo de desarrollo deben venir bien documentada para que el equipo de **DevOps** pueda realizar todo el flujo CI/CD más rápido.

También nos parece super importante dejar nuestro trabajo respaldado, por lo que dejar bien documentada la solución de la prueba te dará puntos extras.

## Etapa 1 Construcción de CI/CD
![cicd](./img/cicd.jpeg)

La automatización es una de nuestras principales pasiones y utilizar las herramientas de **CI/CD** es parte de nuestro día a día. Esta etapa consiste en lo siguiente:

1. Crea un flujo exitoso para la aplicación utilizando la herramienta que mejor domines o consideres.
3. **BONUS**: Utilizar Gitlab-CI te dará puntos extra, ya que es nuestra herramienta de trabajo actual, adicionalmente si pudieras generar la prueba en una rama especifica del mismo proyecto, documentando las configuraciones necesarias para ejecutar este pipeline en un ambiente bien sea onprem o cloud. te dara puntos extras!!.

## Etapa 2 Conceptos basicos, experiencias previas

La automatización es una de nuestras principales pasiones y utilizar las herramientas de **CI/CD** es parte de nuestro día a día. Esta etapa consiste en lo siguiente:

1. ¿En que areas de una empresa puede ser util un perfil DEVOPS?
2. ¿Dentro de los stage de un pipeline cual seria el orden correcto para estructura de los siguientes: build, deploy, package_publish, unit_test, rollback?
3. A nivel de Kubernetes como seria paso a paso la manera correcta de: 1) generar un namespace para un nuevo aplicativo 2) conectarte a dicho kubernetes.
4. **BONUS**: Dockerize la aplicación
![docker](./img/docker.jpeg)

¿Qué pasa con los contenedores? En este momento ya año 2021, los contenedores son un estándar en las implementaciones de aplicaciones **(en la nube o en sistemas locales)**. Entonces, el reto es:
1. Construir la imagen más pequeña que pueda. Escribe un buen Dockerfile :)
2. Ejecutar la app como un usuario diferente de root.


¡Porque cada dia cuenta para crecer, exitos!


# DEVOPS

Archivos agregados al proyecto:
- *.gitlab-ci.yml*
- *Dockerfile*
- *.gitignore*
- *.dockerignore*
- *k8s.yaml*

## Etapa 1
Para la ejecucion dockerizada local del proyecto se debe ejecutar:
- Ejecutar Dockerfile para contruir la imagen
```bash
$ docker build -t node-web-app .
```
- Correr la imagen apuntando a un puerto de su ordenador:
```bash
$ docker run -p 8080:3000 node-web-app
```
¡Listo, su proyecto estará corriendo en http://localhost:8080!

El pipeline se ejecuta en el archivo .gitlab-ci.yml y tiene los siguientes stages:
  - build (npm install)
  - test:jest (npm run test)
  - test:sonar (simulación de análisis sonarqube)
  - package (simulación de contrucción y publicado de imagen a $MY_REGISTRY. Segun el Dockerfile)
  - deploy (simulación de despliegue en kubernetes según reseta k8s.yaml)

## Etapa 2 - Respuestas
1. ¿En que areas de una empresa puede ser util un perfil DEVOPS?
  - El DevOps se encarga de integrar y generar sinergia en todas las areas relacionas al ciclo de vida de software.

2. ¿Dentro de los stage de un pipeline cual seria el orden correcto para estructura de los siguientes: build, deploy, package_publish, unit_test, rollback?
  El orden es:
  - build
  - unit_test
  - package_publish
  - deploy
  - rollback

*Aunque en algunos casos, el "unit_test" puede ser primero al "build" y el "package_publish" puede ser posterior al "deploy"*

3. A nivel de Kubernetes como seria paso a paso la manera correcta de: 1) generar un namespace para un nuevo aplicativo 2) conectarte a dicho kubernetes.

  1) Si el namespace es nuevo. Se debe crear el secreto para poder hacer pull de las imagenes.
     - Comando ejemplo:
```bash
	$ kubectl create namespace development
```
     - Registrando secreto para hacer pull de un registry.
```bash
	$ kubectl create secret docker-registry regsecret --docker-server=my-registry.com:5000 --docker-username=user --docker-password=pass --docker-email=ayvardd@gmail.com -n development
```
  2) Para conectarse al cluster kubernetes se configura con archivo *~/.kube/config* con el usuario y token de acceso.
  
4. **BONUS**: Dockerize la aplicación
- La aplicación fue dockerizada en el archivo *Dockerfile*

1. Construir la imagen más pequeña que pueda. Escribe un buen Dockerfile :)
- El *Dockerfile* adjunto contempla imagenes alpine y un *.dockerignore*. Para evitar cargar archivos innecesarios como el "node_modules".
2. Ejecutar la app como un usuario diferente de root.
- El *Dockerfile* adjunto. Se ejecuta con el usurio "node".
