# Dockerfile to build API Service Container
############################################################
FROM node:10-alpine
# Create app directory
WORKDIR /usr/src/app
COPY ./package*.json ./
RUN npm install
COPY ./ .
USER node
CMD [ "npm", "run" ,"start" ]
EXPOSE 3000
